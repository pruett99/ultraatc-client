﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Input;

namespace FlightEvents.Client
{
    internal class BoundObject
    {
        public BoundObject(MainWindow newmainWindow)
        {
            mainWindow = newmainWindow;
        }
        public void showMessage(string message)
        {
            MessageBox.Show(message, "Alert");
        }

        public void connectPilot(string callsign)
        {
            Debug.WriteLine("Pilot Connecting...");
            mainWindow.ButtonStartTrack_Click(callsign);
        }

        public void disconnectPilot()
        {
            Debug.WriteLine("Pilot Diconnecting...");
            mainWindow.ButtonStopTrack_Click();
        }

        public void connectATC()
        {
            Debug.WriteLine("ATC Connecting...");
            mainWindow.ButtonStartATC_Click();
        }

        public void disconnectATC()
        {
            Debug.WriteLine("ATC Diconnecting...");
            mainWindow.ButtonStopATC_Click();
        }

        public void browserLink(string link)
        {
            Process.Start(new ProcessStartInfo
            {
                UseShellExecute = true,
                FileName = link
            });
        }

        private MainWindow mainWindow { get; set; }
    }
}